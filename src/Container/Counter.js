import React from 'react';
import { connect } from 'react-redux';

import CounterControl from '../Component/Countercontrol';
import CounterOutput from '../Component/CounterOutput';


class Counter extends React.Component{
  // state = {
  //   counter:0
  // };

  // counterChangeHandler = (action) => {
  //   console.log(action)
  //   switch(action){
  //     case 'inc':
  //       this.setState((prevState) => { return { counter: prevState.counter +1 }});
  //       break;
  //     case 'dec':
  //       this.setState((prevState) => { return {  counter:prevState.counter -1 }});
  //       break;
  //     case 'res':
  //       this.setState((prevState) => { return {  counter:prevState.counter * 0 }});
  //       break;    
  //     default:
  //   };
  // };
  render(){
    return(
      <div>
        <CounterOutput value = {this.props.ctr} />
        <CounterControl label = "Increment" clicked = {this.props.onIncrementCounter}/>
        <CounterControl label = "Decrement" clicked = {this.props.onDecrementCounter}/>
        <CounterControl label = "Reset" clicked = {this.props.onResetCounter}/>
      </div>
    )
  };
};

const mapStateToProps = state => {
  return{
    ctr:state.counter
  };
};

const mapDispatchToProps = dispatch => {
  return{
    onIncrementCounter : () => dispatch({type : 'INCREMENT'}),
    onDecrementCounter : () => dispatch({type : 'DECREMENT'}), 
    onResetCounter     : () => dispatch({type : 'RESET'})
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(Counter);